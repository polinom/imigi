'use strict';

angular.module('imigApp')
	.controller('DeskCtrl', function($scope, GameResource, $routeParams) {
		$scope.hand = function() {
			$scope.desk_active = false;
		};

		$scope.desk = function() {
			$scope.desk_active = true;
		};

		$scope.game = GameResource.setGame($routeParams.gameId)
	    $scope.player = GameResource.get_player();
		$scope.hint = GameResource.get_hint;
	});