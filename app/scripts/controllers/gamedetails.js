'use strict';

angular.module('imigApp')
	.controller('GamedetailsCtrl', function($scope, $routeParams, $rootScope, GameResource, Auth) {
		$scope.auth = Auth.getLoginObj();
		$scope.game = GameResource.get_game_by_id($routeParams.gameId);
		// join game logi here
		$scope.join = function() {
			GameResource
				.joinGame($routeParams.gameId)
				.then(function() {
					$rootScope.go('/desk/' + $routeParams.gameId)
				})
		};
	});