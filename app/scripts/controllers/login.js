'use strict';

angular.module('imigApp')
    .controller('LoginCtrl', function($scope, $firebaseSimpleLogin, $location, Auth) {
        $scope.logIn = function() {
            Auth.logIn($scope.email, $scope.password);
        };
    });