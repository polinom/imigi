'use strict';

angular.module('imigApp')
	.controller('CreateGameCtrl', function($scope, $firebase, $location, GameResource) {

		$scope.createGame = function() {
			GameResource
				.crateGame($scope.name, $scope.players, $scope.deck)
				.then(function() {
					$location.path('#/')
				});
		}
	});