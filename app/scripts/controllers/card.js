'use strict';

angular.module('imigApp')
  .controller('CardCtrl', function ($scope, $routeParams, CARD_STATIC_URL) {
    $scope.cardUrl = CARD_STATIC_URL + $routeParams.deck + '/' + $routeParams.img
  });
