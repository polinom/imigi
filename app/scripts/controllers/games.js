'use strict';

angular.module('imigApp')
	.controller('GamesCtrl', function($scope, Auth, GameResource) {
		$scope.loginObj = Auth.getLoginObj();
		$scope.games = GameResource.get_games();
	});