'use strict';

angular.module('imigApp', [
  'firebase',
  'ngRoute',
  'ngAnimate',
  'ngTouch',
  'angular-carousel'
])
  .constant('FIREBASE_URL', 'https://crackling-fire-1217.firebaseio.com/games')
  .constant('CARD_STATIC_URL', 'https://s3.amazonaws.com/imiginarium/')
  .config(function($routeProvider) {
    $routeProvider
    // list of curent active games
    .when('/', {
      templateUrl: 'views/games.html',
      controller: 'GamesCtrl'
    })
      .when('/create', {
        templateUrl: 'views/create_game.html',
        controller: 'CreateGameCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/details/:gameId', {
        templateUrl: 'views/gamedetails.html',
        controller: 'GamedetailsCtrl'
      })
      .when('/desk/:gameId', {
        templateUrl: 'views/desk.html',
        controller: 'DeskCtrl',
        // need this to have user prefetched ready before Controller comes in to game
        resolve: {
          'user': function($q, Auth) {
            return Auth.getLoginObj().$getCurrentUser();
          }
        }
      })
      .when('/card/:deck/:img', {
        templateUrl: 'views/card.html',
        controller: 'CardCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function($rootScope, $window, $location) {

    $rootScope.slide = '';

    $rootScope.back = function(slide) {
      $rootScope.slide = slide || 'slide-right';
      $window.history.back();
    };

    $rootScope.go = function(path, slide) {
      $rootScope.slide = slide || 'slide-left';
      $location.url(path);
    };

    $rootScope.$on('$locationChangeSuccess', function() {
      $rootScope.actualLocation = $location.path();
    });

    $rootScope.$watch(function() {
      return $location.path()
    }, function(newLocation, oldLocation) {
      if ($rootScope.actualLocation === newLocation) {
        $rootScope.slide = 'slide-right';
      }
    });

  });