'use strict';

angular.module('imigApp')
  .service('GameResource', function($firebase, FIREBASE_URL, Auth) {

    var raw_ref = new Firebase(FIREBASE_URL);
    var firebaseRef = $firebase(raw_ref);
    this.auth = Auth.getLoginObj();

    this.setGame = function(key){
      this.curent_game = firebaseRef.$child(key)
      return this.curent_game;
    };

    this.get_games = function() {
      return firebaseRef;
    };

    this.get_game_by_id = function(key) {
      return firebaseRef.$child(key);
    };

    this.crateGame = function(name, players, deck) {
      return firebaseRef.$add({
        name: name,
        players: players,
        deck: deck
      });
    };

    this.joinGame = function(game_key) {
      this.setGame(game_key);
      return this.curent_game.$child('users').$child(this.auth.user.uid).$set({
        username: this.auth.user.email.split('@')[0],
        score: 0,
        hand: [],
      });
    };

    this.get_player = function(){
      return this.curent_game.$child('users').$child(this.auth.user.uid);
    };

    this.get_desk = function(){
      this.get_player
    }

    this.get_hint = 'Pick one of your picture and tell story to other';

  });