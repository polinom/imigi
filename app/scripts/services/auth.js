'use strict';

angular.module('imigApp')
    .factory('Auth', function($firebaseSimpleLogin, $location) {
        var dataRef = new Firebase("https://crackling-fire-1217.firebaseio.com/auth");
        var loginObj = $firebaseSimpleLogin(dataRef, function(error, user){console.log(user)});
        return {
            getLoginObj: function() {
                return loginObj;
            },

            logIn: function(email, password, location) {
                loginObj
                    .$login('password', {
                        email: email,
                        password: password
                    })
                    .then(function(user) {
                        $location.path(location || '#/');
                    }, function(error) {
                        if (error.code === 'INVALID_USER') {
                            loginObj.$createUser(email, password, true).then(function() {
                                loginObj.$login('password', {email: email, password: password }).then(function(){
                                    $location.path(location || '#/');
                                });
                            });
                        } else {
                            alert(error.message);
                        }
                    });
            }
        };
    });