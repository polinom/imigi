'use strict';

describe('Controller: DeskCtrl', function () {

  // load the controller's module
  beforeEach(module('imigApp'));

  var DeskCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DeskCtrl = $controller('DeskCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
