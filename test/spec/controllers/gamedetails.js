'use strict';

describe('Controller: GamedetailsCtrl', function () {

  // load the controller's module
  beforeEach(module('imigApp'));

  var GamedetailsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GamedetailsCtrl = $controller('GamedetailsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
