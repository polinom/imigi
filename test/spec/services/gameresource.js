'use strict';

describe('Service: GameResource', function () {

  // load the service's module
  beforeEach(module('imigApp'));

  // instantiate service
  var GameResource;
  beforeEach(inject(function (_GameResource_) {
    GameResource = _GameResource_;
  }));

  it('should do something', function () {
    expect(!!GameResource).toBe(true);
  });

});
